import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-emp-card',
  templateUrl: './emp-card.component.html',
  styleUrls: ['./emp-card.component.css']
})
export class EmpCardComponent implements OnInit {
  @Input() employee;
  
  constructor(    
    private employeeService : EmployeeService) { }

  ngOnInit() {
  }

  onSelect(){
    this.employeeService.selectEmployee(this.employee);
  }

}
