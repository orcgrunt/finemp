package com.finemp.server.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import com.finemp.server.model.Location;

@RestResource
public interface LocationRepository extends PagingAndSortingRepository<Location, String> {

}