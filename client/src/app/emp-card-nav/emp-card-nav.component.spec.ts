import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpCardNavComponent } from './emp-card-nav.component';

describe('EmpCardNavComponent', () => {
  let component: EmpCardNavComponent;
  let fixture: ComponentFixture<EmpCardNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpCardNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpCardNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
