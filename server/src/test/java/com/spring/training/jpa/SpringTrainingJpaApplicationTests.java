package com.spring.training.jpa;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.results.ResultMatchers;
import org.junit.runner.RunWith;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;


import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringTrainingJpaApplicationTests {

	private MockMvc mockMvc;
	private String employees = "[{\"name\":\"Irvan\",\"gender\":\"male\"},{\"name\":\"Egi\",\"gender\":\"male\"}]";
	private String employee = "{\"name\":\"Irvan\",\"gender\":\"male\"}";
	@Before
	public void setup(){

	}
	
	@Test
	public void getEmployee() throws Exception {
		this.mockMvc.perform(get("/employees?gender=male"))
			.andExpect(content().json(employees));
	}
	
	@Test
	public void postEmployee() throws Exception {
		this.mockMvc.perform(post("/employees")
				.content("{\"name\":\"Satria\",\"gender\":\"male\"}")
				.contentType("application/json"))
				.andExpect(status().isOk());
	}
	
	@Test
	public void getEmployeeById() throws Exception {
		this.mockMvc.perform(get("/employee/1"))
			.andExpect(content().json(employee));
	}
}

