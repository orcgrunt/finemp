create table if not exists locations (
	id varchar(255) not null,
	city varchar(255) not null,
	primary key (id)
);

create table if not exists employees (
    employee_id varchar(255) not null AUTO_INCREMENT,
    first_name varchar(50) not null,
    last_name varchar(50) not null,
    gender varchar(6) not null,
    date_of_birth date not null,
    nationality varchar(45) not null,
    marital_status varchar(7) not null,
    phone varchar(20) not null,
    email varchar(50) not null,
    hired_date date not null,
    suspend_date date,
    division varchar(45) not null,
    grade varchar(45) not null,
    subdivision varchar(45) not null,
    status varchar(20) not null,
    avatar text,
    location_id varchar(255) not null,
    primary key (employee_id),
    foreign key (location_id) references locations(id)
);

