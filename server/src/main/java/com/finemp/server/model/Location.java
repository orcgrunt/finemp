package com.finemp.server.model;

import javax.persistence.*;

@Entity
@Table(name="locations")
public class Location {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(nullable=false)
	private String city;


	public long getLocationId() {
		return id;
	}

	public void setLocationId(long locationId) {
		this.id = locationId;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
}
