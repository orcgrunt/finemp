package com.finemp.server.model;

import java.sql.Date;

import javax.persistence.*;

@Entity
@Table(name="employees")
public class Employee {
	
	public Employee(){}
	
	public Employee(
			String firstName, 
			String lastName, 
			String gender, 
			Date dob,
			String nationality,
			String maritalStatus,
			String phone,
			String subdivision,
			String status,
			Date suspendDate,
			Date hiredDate,
			String grade,
			String division,
			String email,
			String avatar,
			Location location){
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.dateOfBirth = dob;
		this.nationality = nationality;
		this.maritalStatus = maritalStatus;
		this.phone = phone;
		this.subdivision = subdivision;
		this.status = status;
		this.suspendDate = suspendDate;
		this.hiredDate = hiredDate;
		this.grade = grade;
		this.division = division;
		this.email = email;
		this.avatar = avatar;
		this.location = location;
	}
	
	@Id
	@Column(name="employee_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long employeeId;
	
	@Column(name="first_name", nullable = false)
	private String firstName;
	
	@Column(name="last_name", nullable = false)
	private String lastName;
	
	@Column(name="gender", nullable = false)
	private String gender;
	
	@Column(name="date_of_birth", nullable = false)
	private Date dateOfBirth;
	
	@Column(name="nationality", nullable = false)
	private String nationality;
	
	@Column(name="marital_status", nullable = false)
	private String maritalStatus;
	
	@Column(name="phone", nullable = false)
	private String phone;
	
	@Column(name="subdivision", nullable = false)
	private String subdivision;
	
	@Column(name="status", nullable = false)
	private String status;
	
	@Column(name="suspend_date", nullable = true)
	private Date suspendDate;
	
	@Column(name="hired_date", nullable = false)
	private Date hiredDate;
	
	@Column(name="grade", nullable = false)
	private String grade;
	
	@Column(name="division", nullable = false)
	private String division;
	
	@Column(name="email", nullable = false)
	private String email;
	
	@Column(name="avatar")
	private String avatar;
	
	@ManyToOne
	@JoinColumn(name="location_id", nullable = false)
	private Location location;


	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public String getavatar() {
		return avatar;
	}

	public void setavatar(String avatar) {
		this.avatar = avatar;
	}

	public long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getSubdivision() {
		return subdivision;
	}

	public void setSubdivision(String subdivision) {
		this.subdivision = subdivision;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getSuspendDate() {
		return suspendDate;
	}

	public void setSuspendDate(Date suspendDate) {
		this.suspendDate = suspendDate;
	}

	public Date getHiredDate() {
		return hiredDate;
	}

	public void setHiredDate(Date hiredDate) {
		this.hiredDate = hiredDate;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
}
