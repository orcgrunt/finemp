package com.finemp.server.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;

import com.finemp.server.model.Employee;

@RestResource
public interface EmployeeRepository extends PagingAndSortingRepository<Employee,Long> {
    
	public List<Employee> findByFirstNameContainingOrLastNameContainingAllIgnoreCase(
			@Param("firstName") String firstName, 
			@Param("lastName") String lastName, 
			@Param(value = "asc") Sort sort);
	
	@Query("select e from Employee e "
			+ "where (lower(e.firstName) like %:firstName% "
			+ "or lower(e.lastName) like %:lastName%) "
			+ "and lower(e.gender)=lower(:gender)")
	public List<Employee> findByFilterGender(
			@Param("firstName") String firstName,
			@Param("lastName") String lastName,
			@Param("gender") String gender,
			@Param(value = "asc") Sort sort);
	
	@Query("select e from Employee e "
			+ "where (lower(e.firstName) like %:firstName "
			+ "or lower(e.lastName) like %:lastName) "
			+ "and e.location.id=:location")
	public List<Employee> findByFilterLocation(
			@Param("firstName") String firstName,
			@Param("lastName") String lastName,
			@Param("location") Long location,
			@Param(value = "asc") Sort sort);
	
	@Query("select e from Employee e "
			+ "where (lower(e.firstName) like %:firstName "
			+ "or lower(e.lastName) like %:lastName) "
			+ "and lower(e.gender)=:gender "
			+ "and e.location.id=:location")
	public List<Employee> findByFilterGenderAndLocation(
			@Param("firstName") String firstName,
			@Param("lastName") String lastName,
			@Param("gender") String gender,
			@Param("location") Long location,
			@Param(value = "asc") Sort sort);
			
}
