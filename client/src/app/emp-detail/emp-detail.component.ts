import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { EmployeeService } from '../employee.service';
import { LocationService } from '../location.service';

@Component({
  selector: 'app-emp-detail',
  templateUrl: './emp-detail.component.html',
  styleUrls: ['./emp-detail.component.css']
})
export class EmpDetailComponent implements OnInit {

  private genders = ["Male", "Female"];
  private divisions = ["CDC - Red", "CDC - Services", "CDC - TechOne",
                       "MMS - Red", "MMS - Services", "MMS - TechOne",
                       "SWD - Blue", "SWD - TechOne", "SWD - Services"];
  private grades = ["SE - AN", "SE - AP", "SE - PG", "SE - JP"];
  private maritalStatus = ["Single", "Married", "Other"]
  private locations;
  private selectedLocation;
  private employee;
  private form;
  private avatar;
  private defaultAvatar = "../../assets/default.png";

  constructor(
    private employeeService : EmployeeService,
    private locationService : LocationService,
    private formBuilder : FormBuilder
  ) { }

  ngOnInit() {
    this.employeeService.getSelectedEmployee()
      .subscribe(data => {
        this.employee = data;
        this.avatar = this.employee.avatar;
    });

    this.locationService.requestLocations()
      .subscribe(data => {
        this.locations = data;
      });

    this.form = this.formBuilder.group({
        employeeId: this.formBuilder.control(''),
        firstName: this.formBuilder.control('', Validators.compose([Validators.required, Validators.pattern('[\\w\\-\\s\\/]+')])),
        lastName: this.formBuilder.control('', Validators.compose([Validators.required, Validators.pattern('[\\w\\-\\s\\/]+')])),
        gender: this.formBuilder.control('', Validators.compose([Validators.required])),
        dateOfBirth: this.formBuilder.control('', Validators.compose([Validators.required])),
        nationality: this.formBuilder.control('', Validators.compose([Validators.required])),
        maritalStatus: this.formBuilder.control('', Validators.compose([Validators.required])),
        phone: this.formBuilder.control('', Validators.compose([Validators.required, Validators.pattern(/^[0-9\(\)\-\+]{5,25}$/)])),
        subdivision: this.formBuilder.control('', Validators.compose([Validators.required])),
        status: this.formBuilder.control('', Validators.compose([Validators.required])),
        suspendDate: this.formBuilder.control(''),
        hiredDate: this.formBuilder.control('', Validators.compose([Validators.required])),
        grade: this.formBuilder.control('', Validators.compose([Validators.required])),
        division: this.formBuilder.control('', Validators.compose([Validators.required])),
        email: this.formBuilder.control('', Validators.compose([Validators.required, Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)])),
        location: this.formBuilder.control('', Validators.compose([Validators.required])),
        avatar: this.formBuilder.control('')
    })
  }

  restoreForm(){
    this.form.controls['firstName'].setValue(this.employee.firstName);
    this.form.controls['lastName'].setValue(this.employee.lastName);
    this.form.controls['gender'].setValue(this.employee.gender);
    this.form.controls['dateOfBirth'].setValue(this.employee.dateOfBirth);
    this.form.controls['nationality'].setValue(this.employee.nationality);
    this.form.controls['maritalStatus'].setValue(this.employee.maritalStatus);
    this.form.controls['phone'].setValue(this.employee.phone);
    this.form.controls['subdivision'].setValue(this.employee.subdivision);
    this.form.controls['status'].setValue(this.employee.status);
    this.form.controls['suspendDate'].setValue(this.employee.suspendDate);
    this.form.controls['hiredDate'].setValue(this.employee.hiredDate);
    this.form.controls['grade'].setValue(this.employee.grade);
    this.form.controls['division'].setValue(this.employee.division);
    this.form.controls['email'].setValue(this.employee.email);
    this.form.controls['location'].setValue(this.employee.location);
  }

  save(formData){
    formData.location = this.locations.find(l => l.locationId === formData.location);
    formData.avatar = this.avatar;
    this.employeeService.saveEmployee(formData);
  }

  addImage(event){
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.avatar = event.target.result;
    }
    reader.readAsDataURL(event.target.files[0]);
  }
}