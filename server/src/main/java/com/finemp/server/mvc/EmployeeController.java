package com.finemp.server.mvc;

import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.domain.Sort.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.finemp.server.model.Employee;
import com.finemp.server.repository.EmployeeRepository;

@Controller
public class EmployeeController {
	@Autowired
	private EmployeeRepository empRepo;
	
	public EmployeeController() {}
	
	@GetMapping("/employees")
	@ResponseBody
	public Iterable<Employee> getAll(){
		Sort.Order sort = new Sort.Order(Sort.Direction.ASC, "lastName").ignoreCase();
		return empRepo.findAll(new Sort(sort));
	}
	
	@GetMapping("/employees/search")
	@ResponseBody
	public Iterable<Employee> getBySearchTerm(@RequestParam String firstName, @RequestParam String lastName,
			@RequestParam String gender, @RequestParam String location, @RequestParam String sort){
		Sort.Order sorter;
		if(sort.equalsIgnoreCase("desc")) {
			sorter = new Sort.Order(Sort.Direction.DESC, "lastName").ignoreCase();
		} else {
			sorter = new Sort.Order(Sort.Direction.ASC, "lastName").ignoreCase();
		}
		
		if(gender.equals("") && location.equals("")){
			return empRepo.findByFirstNameContainingOrLastNameContainingAllIgnoreCase(
					firstName.toLowerCase(), lastName.toLowerCase(), new Sort(sorter));
		} 
		else if(!gender.equals("") && location.equals("")){
			return empRepo.findByFilterGender(
					firstName.toLowerCase(), lastName.toLowerCase(), gender.toLowerCase(), new Sort(sorter));
		}
		else if(gender.equals("") && !location.equals("")){
			return empRepo.findByFilterLocation(
					firstName.toLowerCase(), lastName.toLowerCase(), Long.parseLong(location), new Sort(sorter));
		}
		else {
			return empRepo.findByFilterGenderAndLocation(
					firstName.toLowerCase(), lastName.toLowerCase(), gender.toLowerCase(), Long.parseLong(location), new Sort(sorter));
		}
	}
	
	@GetMapping("/employee/{id}")
	@ResponseBody
	public Employee getEmployeeById(@PathVariable("id") String id){
		return empRepo.findOne(Long.parseLong(id));
	}
	
	@PostMapping("/employees")
	@ResponseBody
	public Employee postEmployee(@RequestBody Employee emp){
		return empRepo.save(emp);
	}
	
	@PutMapping("/employees")
	@ResponseBody
	public Employee updateEmployee( @RequestBody Employee emp ){
		return empRepo.save(emp);
	}
	
	@DeleteMapping("/employess/{id}")
	@ResponseBody
	public void deleteEmployee(@PathVariable("id") Long id){
		empRepo.delete(id);
	}
}
