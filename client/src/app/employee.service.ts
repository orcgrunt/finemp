import { Injectable } from '@angular/core';
import { Http, URLSearchParams, Headers, RequestOptions } from '@angular/http';
import { BehaviorSubject } from 'rxjs/Rx';
import { Employee } from './model/employee.model';
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/map';


@Injectable()
export class EmployeeService {

  private server;
  private employees = new BehaviorSubject<Employee[]>([]);
  private selectedEmployee = new BehaviorSubject<Employee>(new Employee);

  constructor(private http: Http) { 
    this.server = '/api/employees';
    this.init();
  }
  
  init(){
    this.requestEmployees();
  }

  requestEmployees() {
    this.http.get(this.server)
     .map(response => response.json())
      .subscribe(data => {
        this.employees.next(data);
      });
  }

  getEmployees(){
    return this.employees;
  }

  getBySearchTerm(name, gender, location, sort){
    let url = `${this.server}/search`;

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let searchParams = new URLSearchParams();
    searchParams.append('firstName', name);
    searchParams.append('lastName', name);
    searchParams.append('gender', gender);
    searchParams.append('location', location);
    searchParams.append('sort', sort);

    this.http.get(url, { search: searchParams, headers: headers })
      .map(response => response.json())
        .subscribe(data => {
          this.employees.next(data);
        });
  }

  saveEmployee(employee){
    let body = JSON.stringify(employee);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers : headers });

    if(!employee.employeeId){
      this.http.post(`${this.server}`, body, options)
        .subscribe(() => this.requestEmployees())
    } else {
      this.http.put(`${this.server}`, body, options)
        .subscribe(() => this.requestEmployees())
    }
  }

  selectEmployee(employee){
    this.selectedEmployee.next(employee);
  }

  clearSelectedEmployee() {
    this.selectedEmployee.next(new Employee());
  }

  getSelectedEmployee() {
    return this.selectedEmployee;
  }

  removeEmployee() {
    this.http.delete(`${this.server}/${this.selectedEmployee.value.employeeId}`)
      .subscribe(() => this.requestEmployees());
  }
}
