import { Component, OnInit, ElementRef } from '@angular/core';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-emp-card-nav',
  templateUrl: './emp-card-nav.component.html',
  styleUrls: ['./emp-card-nav.component.css'],
  host: {
    '(document:click)': 'clickOutside($event)',
  },
})
export class EmpCardNavComponent implements OnInit {

  private employees;
  private selectedEmployee: number;
  private selecting;

  constructor(
    private _eref: ElementRef,
    private employeeService: EmployeeService 
  ) {}

  ngOnInit() {
    this.employeeService.getEmployees()
      .subscribe(data => {
        this.employees = data;
    });
  }

  onSelectingEmployee(i) {{
    this.selectedEmployee = i;
    this.selecting = true;
    }
  }

  clickOutside(event) {
    if (!this._eref.nativeElement.contains(event.target)){
      this.selecting = false;
      this.selectedEmployee = -1;
    }
  }

  onAddNewEmployee(){
    this.employeeService.clearSelectedEmployee();
  }
}
