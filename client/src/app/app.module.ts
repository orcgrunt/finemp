import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { MaterialModule } from '@angular/material'
import { BrowserAnimationsModule, NoopAnimationsModule} from '@angular/platform-browser/animations'

import { AppComponent } from './app.component';
import { EmpDetailComponent } from './emp-detail/emp-detail.component';
import { EmpCardComponent } from './emp-card/emp-card.component';
import { HeaderComponent } from './header/header.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { EmpDetailNavComponent } from './emp-detail-nav/emp-detail-nav.component';
import { EmpCardNavComponent } from './emp-card-nav/emp-card-nav.component';
import { EmployeeService } from './employee.service';
import { LocationService } from './location.service';
import { ImageDirective } from './image.directive';
import { DeleteDialogComponent } from './delete-dialog/delete-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    EmpDetailComponent,
    EmpCardComponent,
    HeaderComponent,
    SearchBarComponent,
    EmpDetailNavComponent,
    EmpCardNavComponent,
    ImageDirective,
    DeleteDialogComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    MaterialModule,
    BrowserAnimationsModule, 
    NoopAnimationsModule,
  ],
  providers: [
    EmployeeService,
    LocationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
