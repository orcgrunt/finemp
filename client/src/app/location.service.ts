import { Injectable } from '@angular/core';
import { Http, URLSearchParams, Headers, RequestOptions } from '@angular/http';
import { BehaviorSubject } from 'rxjs/Rx';
import { Location } from './model/location.model';
import 'rxjs/add/operator/map';

@Injectable()
export class LocationService {

  private server;
  private location = new BehaviorSubject<Location>(new Location);

  constructor(private http: Http) { 
    this.server = '/api/locations';
    this.init();
  }

  init(){
    this.requestLocations();
  }

  requestLocations() {
    return this.http.get(this.server)
     .map(response => response.json());
  }
}
