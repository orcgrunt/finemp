import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-delete-dialog',
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.css'],
})
export class DeleteDialogComponent implements OnInit {

  @Input() pop;
  @Output() removePopUp = new EventEmitter();

  constructor(
    private employeeService : EmployeeService
  ) { }

  ngOnInit() {

  }

  onCancelClick(){
    this.removePopUp.emit();
  }

  onOkClick() {
    this.employeeService.removeEmployee();
    this.removePopUp.emit();
  }
}
