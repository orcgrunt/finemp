package com.finemp.server.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.finemp.server.model.Location;
import com.finemp.server.repository.LocationRepository;

@RestController
public class LocationController {

	@Autowired
	private LocationRepository locRepo;
	
	public LocationController(LocationRepository locationRepo) {
		// TODO Auto-generated constructor stub
		this.locRepo = locationRepo;
	}
	
	@GetMapping("/locations")
	@ResponseStatus(HttpStatus.OK)
	public Iterable<Location> allLocations() {
		return locRepo.findAll();
	}
}