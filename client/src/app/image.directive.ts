import { Directive, HostBinding, HostListener, Output, ElementRef, EventEmitter } from '@angular/core';

@Directive({
  selector: '[image-directive]'
})
export class ImageDirective {

  constructor() { }

  @HostBinding('class.is-image-hovering') hovering = false;

  @HostListener('mouseenter') onMouseEnter(){
    this.hovering = true;
  }

  @HostListener('mouseleave') onMouseLeave(){
    this.hovering = false;
  }
}
