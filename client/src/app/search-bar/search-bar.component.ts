import { Component, OnInit, Input, ElementRef } from '@angular/core';
import { FormControl, FormBuilder, FormGroup } from '@angular/forms';
import { EmployeeService } from '../employee.service';
import { LocationService } from '../location.service';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css'],
  host: {
    '(document:click)': 'closeFilter($event)',
  },
})
export class SearchBarComponent implements OnInit {
  @Input() selecting;

  private numberOfEmployees;
  private pop = false;
  private searchControl = new FormControl();
  private sort = "asc";
  private genders = ["Male", "Female"];
  private showFilterMenu = false;
  private filterForm;
  private locations;
  private filterGender;
  private filterLocation;
  private name;

  constructor(
    private employeeService : EmployeeService,
    private locationService : LocationService,
    private formBuilder: FormBuilder,
    private _eref: ElementRef
  ) { }

  ngOnInit() {
    
    this.filterForm = this.formBuilder.group({
      gender: this.formBuilder.control(''),
      location: this.formBuilder.control('')
    })

    this.employeeService.getEmployees()
    .subscribe(data => {
      this.numberOfEmployees = data.length;
    });

    this.locationService.requestLocations()
    .subscribe(data => {
      this.locations = data;
    })

    this.searchControl.valueChanges
    .debounceTime(200)
    .subscribe(name => {
      this.name = name;
      this.getEmployee();
    });
  }

  toggleSort(){
    if(this.sort == "asc"){
      this.sort = "desc";
    } else {
      this.sort = "asc";
    }
    this.getEmployee();
  }

  toggleFilterMenu() {
    if(this.showFilterMenu){
      this.showFilterMenu = false;
    } else {
      this.showFilterMenu = true;
    }
  }

  closeFilter(event) {
    if (!this._eref.nativeElement.contains(event.target)){
      
    }
  }

  onRemovePopUp(){
    this.pop = false;
  }
  onDeleteClick(){
    this.pop = true;
  }

  setFilter(formData){
    this.filterGender = formData.gender;
    this.filterLocation = formData.location;
    this.getEmployee();
    this.showFilterMenu = false;
  }

  clearFilter(){
    this.filterGender = "";
    this.filterLocation = "";

    this.getEmployee();
    this.showFilterMenu = false;
  }

  getEmployee() {
    this.employeeService.getBySearchTerm(
      this.name? this.name : "", 
      this.filterGender? this.filterGender : "", 
      this.filterLocation? this.filterLocation : "", 
      this.sort);
  }

}
