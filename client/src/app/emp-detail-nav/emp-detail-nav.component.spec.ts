import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpDetailNavComponent } from './emp-detail-nav.component';

describe('EmpDetailNavComponent', () => {
  let component: EmpDetailNavComponent;
  let fixture: ComponentFixture<EmpDetailNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpDetailNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpDetailNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
