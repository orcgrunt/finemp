import { FinempPage } from './app.po';

describe('finemp App', () => {
  let page: FinempPage;

  beforeEach(() => {
    page = new FinempPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
